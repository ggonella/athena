

#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationToolMT.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationChain.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationBaseHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationL1CaloHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationFastCaloHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationFastPhotonHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationPrecisionCaloHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationPrecisionElectronHypoTool.h"
#include "TrigEgammaEmulationToolMT/TrigEgammaEmulationPrecisionPhotonHypoTool.h"

DECLARE_COMPONENT( Trig::TrigEgammaEmulationChain )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationToolMT )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationBaseHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationL1CaloHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationFastCaloHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationFastPhotonHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationPrecisionCaloHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationPrecisionElectronHypoTool )
DECLARE_COMPONENT( Trig::TrigEgammaEmulationPrecisionPhotonHypoTool )


