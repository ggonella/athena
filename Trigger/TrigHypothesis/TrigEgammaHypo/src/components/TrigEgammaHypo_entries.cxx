#include "../TrigEgammaFastCaloHypoAlgMT.h"
#include "../TrigEgammaFastCaloHypoToolInc.h"
#include "../TrigEgammaPrecisionCaloHypoToolInc.h"
#include "../TrigEgammaPrecisionTrackingHypoToolInc.h"
#include "../TrigEgammaPrecisionPhotonHypoToolInc.h"
#include "../TrigEgammaPrecisionElectronHypoToolInc.h"
#include "../TrigEgammaFastElectronHypoToolInc.h"
#include "../TrigEgammaFastElectronHypoAlgMT.h"
#include "../TrigEgammaFastPhotonHypoAlgMT.h"
#include "../TrigEgammaFastPhotonHypoToolInc.h"
#include "../TrigEgammaPrecisionCaloHypoAlgMT.h"
#include "../TrigEgammaPrecisionTrackingHypoAlgMT.h"
#include "../TrigEgammaPrecisionPhotonHypoAlgMT.h"
#include "../TrigEgammaPrecisionElectronHypoAlgMT.h"
#include "../combos/TrigEgammaMassHypoTool.h"
#include "../combos/TrigEgammaDPhiHypoTool.h"

DECLARE_COMPONENT( TrigEgammaFastCaloHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaFastElectronHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaFastPhotonHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaFastCaloHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionCaloHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionTrackingHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionPhotonHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionElectronHypoToolInc )
DECLARE_COMPONENT( TrigEgammaFastElectronHypoToolInc )
DECLARE_COMPONENT( TrigEgammaFastPhotonHypoToolInc )
DECLARE_COMPONENT( TrigEgammaPrecisionCaloHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaPrecisionTrackingHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaPrecisionPhotonHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaPrecisionElectronHypoAlgMT )
DECLARE_COMPONENT( TrigEgammaMassHypoTool )
DECLARE_COMPONENT( TrigEgammaDPhiHypoTool )
